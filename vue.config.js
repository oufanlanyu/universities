module.exports = {
    lintOnSave: false,
    devServer: {
        port: 8080,
        proxy: {
            '/jeecg-boot': {
                //  target: 'http://47.92.31.152:8080',  //请求线上
                // target: 'http://localhost:8080',
                // target: 'http://192.168.1.18:8080',     // 大电脑
                target: 'http://192.168.1.17:8080',     // 马
                // target: 'http://192.168.1.15:8090',     // 邱
                ws: false,
                changeOrigin: true
            }
        }
    }
}
