// 邮箱验证
function Ruleemail(rule, value, callback) {
    let mal = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!value) {
        return callback(new Error('请输入正确的邮箱'));
    }
    if (!(mal.test(value))) {
        callback(new Error('请输入正确的邮箱'));
    } else {
        callback();
    }
};
// 手机号码验证
function Rulephone(rule, value, callback) {
    let regFormat = /^[1][3578][0-9]{9}$/; //正确手机号
    if (!value) {
        return callback(new Error('请输入正确手机号'));
    }
    if (!(regFormat.test(value))) {
        callback(new Error('请输入正确手机号'));
    } else {
        if (value < 18) {
            callback(new Error('必须大于18岁'));
        } else {
            callback();
        }
    }
};

export default{
    Ruleemail,
    Rulephone
}