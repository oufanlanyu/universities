import api from '@/api/login.js'
import util from './index'
import Vue from 'vue'
import Router from 'vue-router'
import { Message } from 'element-ui'

Vue.use(Router)
export default {
    // 退出登陆
    logOut (){
        var _this = this;
        if(util.getToken()){
            api.logout(
                util.getToken()
            ).then(res => {
                if(res.code == 200){
                    Message.success(res.message)
                    util.removeToken(util.getToken())
                    localStorage.removeItem('userid');
                    localStorage.removeItem('store');
					localStorage.removeItem('userInfo');
                }
            }).catch(err => {
                console.log(err)
            })
        }else{
            console.log('------------- token不存在 --------------')
        }
    }
}
