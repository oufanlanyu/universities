
let util = {
    // 获取token
    getToken (){
        return localStorage.getItem('pro__Access-Token')
    },
    // 设置token
    setToken (token){
        localStorage.setItem('pro__Access-Token', token);
    },
    // 删除token
    removeToken (){
        localStorage.removeItem('pro__Access-Token');
    },
    // 获取userid
    getuserid(){
        return localStorage.getItem('userid')
    },
}

export default util
