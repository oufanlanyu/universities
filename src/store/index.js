import Vue from 'vue'
import Vuex from 'vuex'
import util from '@/utils/index'
// import user from './modules/user/user'
Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		// user,
	},
	state: {
        AccessToken: util.getToken() ? util.getToken() : '',
		isLogin: false, //登陆状态
		// user
		userid: '',     //用户id
		userinfo: {},   //用户信息
		userdepart: {}, //院系信息
		sourceinfo: {}, //生源信息
		resumeinfo: {}, //简历信息
		resumeid: '',   //简历id,
		entercoding: '',  //企业编码
		// school
		newscategory: [], // 新闻分类
		employmentcategory: [], // 就业服务分类
		biogeniccategory: [], // 生源信息分类
		servicecategory: [], // 服务指南分类
        policiescategory: [], // 政策法规分类
        // company
        tenantId: '',       // 单位服务商id
    },
	mutations: {
        // 修改token，并将token存入localStorage
		setLoginTonken(state, data) {
			state.AccessToken = data;
			util.setToken(data);
		},
		// 修改用户登陆装态
		setisLogin(state, data) {
			state.isLogin = data;
		},
		// 设置用户ID,储存到本地储存中
        setUserId(state, id) {
			state.userid = id,
			localStorage.setItem('userid', id);
        },
		// 储存用户数据
		setUserData(state, data) {
			state.userinfo = data;
		},
		setUserDepart(state, data){
			state.userdepart = data;
		},
		// 储存用户生源信息的数据
		setSourceData(state, data) {
			state.sourceinfo = data;
		},
		// 储存用户简历信息的数据
		setResumeData(state, data) {
			state.resumeinfo = data;
		},
		// 储存用户简历信息的数据
		setResumeId(state, data) {
			state.resumeid = data;
		},
		// 储存企业编码
		setEntercoding(state, data) {
			state.entercoding = data;
        },
        // 储存单位服务商id
        settenantId(state, data){
            state.tenantId = data;
        },
		// 新闻资讯分类
		setNewsCategory(state, data) {
			state.newscategory = data;
		},
		// 新闻资讯分类
		setEmploymentCategory(state, data) {
			state.employmentcategory = data;
		},
		// 生源信息分类
		setBiogenicCategory(state, data) {
			state.biogeniccategory = data;
		},
		// 服务指南分类
		setserviceCategory(state, data) {
			state.servicecategory = data;
		},
		// 服务指南分类
		setPoliciesCategory(state, data) {
			state.policiescategory = data;
		}
    }
})
