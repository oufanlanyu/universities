let user = {
    state: {
        userid: '',
        userinfo: {}
    },
    mutations: {
        // 设置用户ID,储存到本地储存中
        setUserId(state, id) {
			state.userid = id,
			localStorage.setItem('userid', id);
        },
		// 储存用户数据
		setUserData(state, data) {
			state.userinfo = data;
		}
	}
}
export default {
    user
}