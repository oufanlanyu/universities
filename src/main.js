import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import router from './router/index'
import store from './store/index'
import 'font-awesome/css/font-awesome.min.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import MetaInfo from 'vue-meta-info'
import axios from 'axios'
import Storage from 'vue-ls';
import "babel-polyfill";
import './assets/css/reset.css';
import './assets/css/common.styl';
import './assets/css/school/comon.styl';
import './assets/css/company.styl';
import moment from 'moment'
import VueAMap from 'vue-amap'
import { Base64 } from 'js-base64';

/******* swiper组件 ******/
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper, /* { default global options } */)
/************************/

Vue.use(VueAMap);
Vue.use(MetaInfo)
Vue.use(ElementUI)
Vue.use(Vuex)
Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.use(Storage)
Vue.prototype.$moment = moment;
moment.locale('zh-cn');

VueAMap.initAMapApiLoader({
	key: 'd5e9ec3306fa69e895daa6dcad359cec',
	plugin: ['AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType'],
	v: '1.4.4'
});

// 跳转路由时回到顶部
router.afterEach((to, from, next) => {
	window, scrollTo(0, 0)
});

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
