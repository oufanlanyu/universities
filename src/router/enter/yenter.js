export default [
    // 单位注册
    {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "register" */ '@/views/enter/differ/Register.vue'),
        meta: {
            isAuthRequired: false
        },
    }
]
