export default [
    // 单位管理
    {
        path: '/enter',
        name: 'enter',
        redirect: '/enter/index',
        component: () => import('@/views/company/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            //首页
            {
                path: 'index',
                name: 'index',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/Index.vue')
            },
            {
                path: 'compyinfo',
                name: 'compyinfo',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/comption/Compyinfo.vue')
            },
             //入驻学校
            {
                path: 'setschool',
                name: 'setschool',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/comption/Setschool.vue')
            },
             //添加产品
            {
                path: 'addproduct',
                name: 'addproduct',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/comption/Addproduct.vue')
            },
            //添加新闻
            {
                path: 'addnews',
                name: 'addnews',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/comption/Addnews.vue')
            },
             //招聘岗位
             {
                path: 'recruitpost',
                name: 'recruitpost',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Recruitpost.vue')
            },
             //招聘岗位>审核状态
            {
                path: 'auditstatus',
                name: 'auditstatus',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Auditstatus.vue')
            },
             //发布岗位
            {
                path: 'relepost',
                name: 'relepost',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Relepost.vue')
            },
            //招聘会
            {
                path: 'jobfair',
                name: 'jobfair',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Jobfair.vue')
            },
            // 双选会
            {
                path: 'doublemeet',
                name: 'doublemeet',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Doublemeet.vue')
            },
            //宣讲会
            {
                path: 'preachmeet',
                name: 'preachmeet',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Preachmeet.vue')
            },
            //发布宣讲会
            {
                path: 'relemeet',
                name: 'relemeet',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Relemeet.vue')
            },
            // 查看宣讲会报名人数和信息
            {
                path: 'preachmeetnum',
                name: 'preachmeetnum',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/recruitment/Preachmeetnum.vue')
            },
            //候选人
            //简历中心
            {
                path: 'reccenter',
                name: 'reccenter',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/cand/Reccenter.vue')
            },
            //面试日程
            {
                path: 'interschel',
                name: 'interschel',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/cand/Interschel.vue')
            },
            //关注我的人才
            {
                path: 'followper',
                name: 'followper',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/cand/Followper.vue')
            },
            //设置中心
            //消息通知
            {
                path: 'messageinfo',
                name: 'messageinfo',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/setup/Messageinfo.vue')
            },
            //消息详情
            {
                path: 'mesdetail',
                name: 'mesdetail',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/setup/Mesdetail.vue')
            },
            //账号绑定
            {
                path: 'accbind',
                name: 'accbind',
                meta: {
                    isAuthRequired: true
                },
                component: () => import('@/views/company/setup/Accbind.vue')
            },
        ]
    },
     //注册信息
    {
        path: '/perfectinfo',
        name: 'perfectinfo',
        meta: {
            isAuthRequired: true
        },
        component: () => import('@/views/company/home/Perfectinfo.vue')
    },
]
