export default [
    //企业管理
    {
        path: '/enter',
        name: 'enter',
        component: () => import(/* webpackChunkName: "enter" */ '@/views/enter/home/Layout.vue'),
        meta: {
            isAuthRequired: false
        },
        children: [
            //首页
            {
                path: '/enter',
                name: 'enter',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/index.vue')
            },
            // 联系人资料
            {
                path: '/enter_home',
                name: 'enter_home',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_home.vue')
            },
            // 实习签到
            {
                path: '/enter_internship',
                name: 'enter_internship',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_internship.vue')
            },
            //实习签到列表
            {
                path: '/enter_internList',
                name: 'enter_internList',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_internList.vue')  
            },
            // 账号认证绑定
            {
                path: '/enter_system',
                name: 'enter_system',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/system.vue')
            },
            // 发布宣讲会
            {
                path: '/recruit_home',
                name: 'recruit_home',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/recruit_home.vue')
            },
            // 企业主页
            {
                path: '/enter_data',
                name: 'enter_data',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_data.vue')
            },
            //企业介绍
             {
                path: '/enter_introduce',
                name: 'enter_introduce',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_introduce.vue')
            },
            //企业福利
            {
                path: '/enter_welfare',
                name: 'enter_welfare',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_welfare.vue')
            },
            //企业相册
               {
                path: '/enter_album',
                name: 'enter_album',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_album.vue')
            },
            //企业产品
              {
                path: '/enter_product',
                name: 'enter_product',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_product.vue')
            },
            //企业新闻
            {
                path: '/enter_news',
                name: 'enter_news',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_news.vue')
            },
            // 发布宣讲会
            {
                path: '/enter_preach',
                name: 'enter_preach',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_preach.vue')
            },
            // 发布职位
            {
                path: '/enter_position',
                name: 'enter_position',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_position.vue')  
            },
 
            //发布宣讲会列表
             {
                path: '/enter_preachList',
                name: 'enter_preachList',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_preachList.vue')  
            },
            //招聘会列表
            {
                path: '/enter_recruit',
                name: 'enter_recruit',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_recruit.vue')  
            },
            //双选会列表
            {
                path: '/enter_douElection',
                name: 'enter_douElection',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_douElection.vue')  
            },

             //简历中心
             {
                path: '/enter_resumeList',
                name: 'enter_resumeList',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_resumeList.vue')  
            },
            //新闻增加
               {
                path: '/add_enterNews',
                name: 'add_enterNews',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/add_enterNews.vue')  
            },
            //产品增加
               {
                path: '/add_product',
                name: 'add_product',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/add_product.vue')  
            },
            //招聘岗位
                {
                path: '/enter_positionMgement',
                name: 'enter_positionMgement',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_positionMgement.vue')  
            },
              //招聘岗位审核
                {
                path: '/enter_positionS',
                name: 'enter_positionS',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_positionS.vue')  
            },
            //岗位子页面
               {
                path: '/mgement',
                name: 'mgement',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_positionS.vue')  
            },
            //请假申请
               {
                path: '/enter_leaveManagement',
                name: 'enter_leaveManagement',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_leaveManagement.vue')  
            },
            //面试日程
             {
                path: '/enter_programme',
                name: 'enter_programme',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_programme.vue')  
            },
            //关注我的人才
             {
                path: '/enter_follow',
                name: 'enter_follow',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_follow.vue')  
            },
            //企业入驻学校
             {
                path: '/enter_entSchSettledin',
                name: 'enter_entSchSettledin',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_entSchSettledin.vue')  
            },
            //查看签到
            {
                path: '/enter_check',
                name: 'enter_check',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_check.vue')  
            },
            //实习日志
            {
                path: '/enter_Diary',
                name: 'enter_Diary',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_Diary.vue')  
            },
            //查看日志
            {
                path: '/enter_checkdiary',
                name: 'enter_checkdiary',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_checkdiary.vue')  
            },
            //职务列表
            {
                path: '/enter_sys_position',
                name: 'enter_sys_position',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_sys_position.vue')  
            },
            //角色维护      
              {
                path: '/roleUserList',
                name: 'roleUserList',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleUserList.vue')  
            },
            //用户权限表
               {
                path: '/RoleUser',
                name: 'RoleUser',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleUser.vue')  
            },
            //用户角色表   
             {
                path: '/RoleManager',
                name: 'RoleManager',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleManager.vue')  
            },
            //菜单管理
             {
                path: '/RoleMenu',
                name: 'RoleMenu',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleMenu.vue')  
            },
            //系统通知
            {
                path: '/enter_listAnt',
                name: 'enter_listAnt',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_listAnt.vue')  
            },
            //部门管理
            {
                path: '/RoleDeptManager',
                name: 'RoleDeptManager',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleDeptManager.vue')  
            },
            //部门用户管理
            {
                path: '/RoleDeptUserManger',
                name: 'RoleDeptUserManger',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/sys/RoleDeptUserManger.vue')  
            },            //部门用户管理
            {
                path: '/enter_register',
                name: 'enter_register',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/enter/home/enter_register.vue')  
            }
        
        ]
    }
]