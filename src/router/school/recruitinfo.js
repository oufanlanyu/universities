export default [
    // 学校定制页面/招聘信息
    {
        path: '/',
        name: 'index',
        component: () => import(/* webpackChunkName: "user" */ '@/views/Effect.vue'),
        meta: {
            isAuthRequired: false
        },
        children: [
            // 校内招聘会
            {
                path: '/oncampus',
                name: 'oncampus',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Oncampus.vue')
            },
            // 校外招聘会
            {
                path: '/offcampus',
                name: 'offcampus',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Offcampus.vue')
            },
            // 宣讲会
            {
                path: '/preachmeet',
                name: 'preachmeet',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Preachmeet.vue')
            },
            // 事业单位招聘   
            {
                path: '/publicinst',
                name: 'publicinst',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Publicinst.vue')
            },
            // 双选会
            {
                path: '/dbelection',
                name: 'dbelection',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Dbelection.vue')
            },
            // 校外招聘会/事业单位招聘详情
            {
                path: '/offpublicdetail/:id',
                name: 'offpublicdetail',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/components/Offpublicdetail.vue'),
                props: true
            },
            //招聘会详情
            {
                path: '/recdetl/:id',
                name: 'recdetl',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Rec_detl.vue'),
                props: true
            },
            //宣讲会详情
            {
                path: '/preachmeetdetail/:id',
                name: 'preachmeetdetail',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Preachmeetdetail.vue'),
                props: true
            },
            //参会单位
            {
                path: '/partunit/:id',
                name: 'partunit',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Part_unit.vue'),
                props: true
            },
            //宣讲会详情
            {
                path: '/preach',
                name: 'preach',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Preach.vue')
            },
            //宣讲/招聘/双选会报名
            {
                path: '/recsign/:id',
                name: 'recsign',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/recruit/Rec_sign.vue'),
                props: true
            },
        ]
    },
]