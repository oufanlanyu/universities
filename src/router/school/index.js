export default [
    // 学校定制页面
    {
        path: '/',
        name: 'index',
        component: () => import(/* webpackChunkName: "user" */ '@/views/Effect.vue'),
        meta: {
            isAuthRequired: false
        },
        children: [
            // 学校首页
            {
                path: '/',
                name: 'index',
                meta: {
                    isAuthRequired: false
                },
                component: () => import(/* webpackChunkName: "user" */ '@/views/school/Index.vue')
            },
            // 学校简介
            {
                path: '/schoolinfo',
                name: 'schoolinfo',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/survey/Schoolinfo.vue')
            },
            //院系风采
            {
                path: '/brief',
                name: 'brief',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/survey/Brief.vue')
            },
            //院系介绍详情
            {
                path: '/briefdetil/:id',
                name: 'briefdetil',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/survey/Brief_detil.vue'),
                props: true
            },
            //新闻资讯
            {
                path: '/news/:id',
                name: 'news',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/news/News.vue'),
                props: true
            },
            //新闻详情
            {
                path: '/newdetil/:id',
                name: 'newdetil',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/news/New_detil.vue'),
                props: true
            },
            // 就业服务
            {
                path: '/employment/:id',
                name: 'employment',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/employment/Employment.vue'),
                props: true
            },
            // 生源信息
            {
                path: '/biogenic',
                name: 'biogenic',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/biogenic/Biogenic.vue'),
            },
            // 单位新闻列表
            {
                path: '/comnewslist',
                name: 'comnewslist',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/company/Comnewslist.vue'),
            },
            // 政策法规
            {
                path: '/policies/:id',
                name: 'policies',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/policies/Policies.vue'),
                props: true
            },
            //岗位页面
            {
                path: '/post',
                name: 'post',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/position/Post.vue')
            },
            // 正式岗位
            {
                path: '/officialpost',
                name: 'officialpost',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/position/Officialpost.vue')
            },
            // 实习岗位
            {
                path: '/internshippost',
                name: 'internshippost',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/position/Internshippost.vue')
            },
            //岗位详情
            {
                path: '/post_detl/:id',
                name: 'post_detl',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/position/Post_detl.vue'),
                props: true
            },
            // 服务指南
            {
                path: '/serverinfo/:id',
                name: 'serverinfo',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/serverinfo/Serverinfo.vue'),
                props: true
            },
            //联系我们
            {
                path: '/contact',
                name: 'contact',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/Contact.vue')
            },
            // 培训会列表
            {
                path: '/training',
                name: 'training',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/train/Training.vue')
            },
            // 培训会详情
            {
                path: '/trainingdetail/:id',
                name: 'trainingdetail',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/train/Trainingdetail.vue'),
                props: true
            },
            //单位主页
            {
                path: '/company/:id',
                name: 'company',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/Company.vue'),
                props: true
            },
            // 单位新闻
            {
                path: '/comdetail/:id',
                name: 'company',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/company/Comnewdetail.vue'),
                props: true
            },
            // 单位产品
            {
                path: '/comproductdetail/:id',
                name: 'company',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/company/Comproductdetail.vue'),
                props: true
            },
            // 入驻单位
            {
                path: '/settlementunit',
                name: 'settlementunit',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/companylist/Settlementunit.vue')
            },
            // 校友单位
            {
                path: '/alumniunit',
                name: 'alumniunit',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/companylist/Alumniunit.vue')
            },
            //友情链接
            {
                path: '/friends',
                name: 'friends',
                meta: {
                    isAuthRequired: false
                },
                component: () => import('@/views/school/Friends.vue')
            },
            // //问卷调查列表
            // {
            //     path: '/volume',
            //     name: 'volume',
            //     meta: {
            //         isAuthRequired: false
            //     },
            //     component: () => import('@/views/school/Volume.vue')
            // },
        ]
    },
     //问卷调查详情
    {
        path: '/volume',
        name: 'volume',
        meta: {
            isAuthRequired: false
        },
        component: () => import('@/views/school/Volume.vue')
    },
]
