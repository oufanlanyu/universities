import Vue from 'vue'
import Router from 'vue-router'
import util from '@/utils/index'
import { Message } from 'element-ui'
/**
 * 个人中心路由
 */
import main from './main' //主要路由
import career from './user/career' //个人生涯
import user from './user/user' //简历管理
import job from './user/job' //求职管理
import campus from './user/campus' //校园招聘
import internship from './user/internship' //实习管理
import train from './user/train' //培训活动
import question from './user/question' //问卷调查
import message from './user/message' //消息通知
import account from './user/account' //账号管理
/**
 * 企业中心路由
 */
import enter from './enter/enter' //企业中心
import yenter from './enter/yenter' //企业中心改

import layout from './enter/layout' //新企业中心
//学校路由
import school from './school/index' //学校
import recruitinfo from './school/recruitinfo' //招聘信息


Vue.use(Router)
let routes = new Set([...main, ...career, ...user, ...job, ...campus, ...internship, ...train, ...question, ...message, ...account, ...enter, ...yenter, ...school, ...recruitinfo, ...layout]);

const router = new Router({
	routes,
	mode: 'history'
})

// 导航守卫
// 使用 router.beforeEach 注册一个全局前置守卫，判断用户是否登陆
router.beforeEach((to, from, next) => {
	let token = util.getToken();
	if(to.meta.isAuthRequired && !token){
		return next('/')
	}else if(to.path == '/login' && token){
		Message.warning('你已经登录了')
		this.$router.back(-1)
	}else if(to.path == '/enter_Login' && token){
		Message.warning('你已经登录了')
		this.$router.back(-1)
	}else {
		next()
	}
});

export default router;
