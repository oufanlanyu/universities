import Home from '@/views/Home.vue'
export default [
    // 登陆
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/Login.vue')
    },
    {
        path: '/enter_Login',
        name: 'enter_Login',
        component: () => import('@/views/enter/differ/enter_Login.vue')
    }
]
