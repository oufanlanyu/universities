export default [
    // 预览简历
    {
        path: '/previewresume/:id',
        name: '/previewresume',
        meta: {
            isAuthRequired: false
        },
        component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/Previewresume.vue'),
        props: true
    },
    // 简历管理
    {
        path: '/user',
        name: 'user',
        redirect: '/user/resume',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/resume/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 我的简历
            {
                path: '/user/resume',
                name: 'user',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/User.vue')
            },
            // 基本信息
            {
                path: 'info',
                name: 'info',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/Info.vue')
            },
            // 修改简历
            {
                path: 'createresume',
                name: 'createresume',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/Createresume.vue')
            },
            // 简历外发
            {
                path: 'resumedevel',
                name: 'resumedevel',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/Resumedevel.vue')
            },
            // 隐私设置
            {
                path: 'privsetting',
                name: 'privsetting',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/resume/Privsetting.vue')
            },


        ]
    }
]
