export default [
    // 培训活动
    {
        path: '/train',
        name: 'train',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/train/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 报名活动
            {
                path: '/train',
                name: 'train',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/train/Train.vue')
            },
            // 已参加活动
            {
                path: 'intrain',
                name: 'intrain',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/train/Intrain.vue')
            },
            // 未参加活动
            {
                path: 'untrain',
                name: 'untrain',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/train/Untrain.vue')
            },
        ]
    }
]