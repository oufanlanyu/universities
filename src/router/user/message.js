export default [
    // 通知中心
    {
        path: '/message',
        name: 'message',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/message/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 全部通知
            {
                path: '/message',
                name: 'message',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Message.vue')
            },
            // 职位通知
            {
                path: 'jobmessage',
                name: 'jobmessage',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Jobmessage.vue')
            },
            // 系统通知
            {
                path: 'sysmessage',
                name: 'sysmessage',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Sysmessage.vue')
            },
            // 活动通知
            {
                path: 'partmessage',
                name: 'partmessage',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Partmessage.vue')
            },
            // 作业通知
            {
                path: 'taskmessage',
                name: 'taskmessage',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Taskmessage.vue')
            },
            // 审核通知
            {
                path: 'checkmessage',
                name: 'checkmessage',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/message/Checkmessage.vue')
            },
        ]
    }
]
