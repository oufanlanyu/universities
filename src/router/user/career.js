export default [
    // 个人生涯
    {
        path: '/career',
        name: 'career',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/career/Layout.vue'),
        redirect: '/career/workexper',
        meta: {
            isAuthRequired: true
        },
        children: [
            // 工作经历
            {
                path: '/career/workexper',
                name: 'workexper',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/career/Workexper.vue')
            },
            // 我的作品
            {
                path: '/career/myworke',
                name: 'myworke',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/career/Myworke.vue')
            },
            // 获得奖励
            {
                path: '/career/getrewards',
                name: 'getrewards',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/career/Getrewards.vue')
            },
            // 资格证书
            {
                path: '/career/certificate',
                name: 'certificate',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/career/Certificate.vue')
            },
            // 参加活动
            {
                path: '/career/joinpart',
                name: 'joinpart',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/career/Joinpart.vue')
            },
        ]
    }
]
