export default [
    // 实习管理
    {
        path: '/internship',
        name: 'internship',
        redirect: '/internship/activity',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/internship/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 实习活动
            {
                path: 'activity',
                name: 'activity',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Activity.vue')
            },
            // 实习签到
            {
                path: 'signin',
                name: 'signin',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Signin.vue')
            },
            // 实习日志
            {
                path: 'journal',
                name: 'journal',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Journal.vue')
            },
            // 新建日志
            {
                path: 'newlog',
                name: 'newlog',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Newlog.vue'),
            },
            // 修改日志
            {
                path: 'editlog/:id',
                name: 'editlog',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Newlog.vue'),
                props: true
            },
            // 请假申请
            {
                path: 'leaverequest',
                name: 'leaverequest',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Leaverequest.vue')
            },
            // 新建请假
            {
                path: 'newleavere',
                name: 'newleavere',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Newleavere.vue'),
            },
            // 修改请假
            {
                path: 'editleavere/:id',
                name: 'editleavere',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Newleavere.vue'),
                props: true
            },
            // 实习作业
            {
                path: 'practicework',
                name: 'practicework',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Practicework.vue')
            },
            // 添加作业
            {
                path: 'addoperation',
                name: 'addoperation',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Addoperation.vue'),
            },
            // 修改作业
            {
                path: 'editoperation/:id',
                name: 'editoperation',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Addoperation.vue'),
                props: true
            },
            // 实习评价
            {
                path: 'evaluate',
                name: 'evaluate',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Evaluate.vue')
            },
            // 发布评价
            {
                path: 'pushevaluate/:id',
                name: 'pushevaluate',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Pushevaluate.vue'),
                props: true
            },
            // 自主实习
            {
                path: 'autopractice',
                name: 'autopractice',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Autopractice.vue'),
            },
            // 申请/添加自主实习
            // 添加作业
            {
                path: 'applyautoprac',
                name: 'applyautoprac',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/form/Applyautoprac.vue'),
            },
            // 三方协议
            {
                path: 'threeparty',
                name: 'threeparty',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/internship/Threeparty.vue'),
            },
        ]
    }
]
