export default [
    // 问卷调查
    {
        path: '/questionaire',
        name: 'questionaire',
        redirect:'/questionaire/mynaire',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/question/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 我的问卷
            {
                path: 'mynaire',
                name: 'mynaire',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/question/Mynaire.vue')
            },
            // 已完成问卷
            {
                path: 'Complaire',
                name: 'Complaire',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/question/Complaire.vue')
            },
            // 未完成问卷
            {
                path: 'uncomplaire',
                name: 'uncomplaire',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/question/Uncomplaire.vue')
            },
        ]
    }
]
