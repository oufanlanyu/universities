export default [
    // 校园招聘
    {
        path: '/campus',
        name: 'campus',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/campus/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 我的招聘会
            {
                path: '/campus',
                name: 'campus',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/campus/Campus.vue')
            },
            // 我的宣讲会
            {
                path: '/campus/preach',
                name: 'preach',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/campus/Preach.vue')
            },
            // 我的双选会
            {
                path: '/campus/dbelection',
                name: 'dbelection',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/campus/Dbelection.vue')
            },
        ]
    }
]