export default [
    // 简历管理
    {
        path: '/job',
        name: 'job',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/job/Layout.vue'),
        redirect: '/job/mypost',
        meta: {
            isAuthRequired: true
        },
        children: [
            // 我的职位
            {
                path: '/job/mypost',
                name: 'job',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/job/Mypost.vue')
            },
            // 我的关注
            {
                path: '/job/myconcern',
                name: 'myconcern',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/job/Myconcern.vue')
            },
        ]
    }
]