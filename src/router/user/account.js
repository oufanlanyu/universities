export default [
    // 账号管理
    {
        path: '/account',
        name: 'account',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/account/Layout.vue'),
        meta: {
            isAuthRequired: true
        },
        children: [
            // 编辑资料
            {
                path: '/account',
                name: 'account',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "user" */ '@/views/user/account/Editmeans.vue')
            },
            // 手机号码
            {
                path: 'phone',
                name: 'phone',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/account/Account.vue')
            },
            // 密码设置
            {
                path: 'password',
                name: 'password',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/account/Password.vue')
            },
            // 微信绑定
            {
                path: 'wechat',
                name: 'wechat',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/account/Wechat.vue')
            },
            // 邮箱绑定
            {
                path: 'email',
                name: 'email',
                meta: {
                    isAuthRequired: true
                },
                component: () => import(/* webpackChunkName: "info" */ '@/views/user/account/Email.vue')
            },
        ]
    }
]