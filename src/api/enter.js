import Vue from 'vue'
import { axios } from '@/utils/request'

const enter = {
    //通过所属部门查询企业基本信息
    pageEnterprise(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/list',
            method: 'get',
            params: data,
        })
    },
    //增加企业基本信息
    addenterinfo(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/add',
            method: 'post',
            data: data
        })
    },
    //修改基本信息
    enitenterinfo(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/edit',
            method: 'put',
            data: data
        })
    },
    // 简历投递-分页列表查询
    queryApplyPostList(data) {
        return axios({
            url: '/apply/applyPosition/list',
            method: 'get',
            params: data,
        })
    },
    // 分页查询简历
    resumeList(data) {
        return axios({
            url: '/resume/resumeInfo/list',
            method: 'get',
            params: data,
        })
    },
    //查询简历信息
    queryById(data) {
        return axios({
            url: '/resume/resumeInfo/queryById',
            method: 'get',
            params: data,
        })
    },
    //修改邀请
    checkifInvite(data) {
        return axios({
            url: '/apply/applyPosition/edit',
            method: 'put',
            data: data
        })
    },
    //删除简历中心
    deleteifInvite(data) {
        return axios({
            url: '/apply/applyPosition/delete',
            method: 'delete',
            params: data,
        })
    },
    //删除职位
    deletePosition(data) {
        return axios({
            url: '/enterprise/positionMgement/delete',
            method: 'delete',
            params: data,
        })
    },
    //企业产品列表查询
    enterProducts(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/list',
            method: 'get',
            params: data,
        })
    },
    //删除企业新闻
    deleteNews(data) {
        return axios({
            url: '/enterprise/corporateNews/delete',
            method: 'delete',
            params: data,
        })
    },
    //新闻增加1
    addNews(data) {
        return axios({
            url: '/enterprise/corporateNews/add',
            method: 'post',
            data: data,
        })
    },
    //产品增加
    addProduct(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/add',
            method: 'post',
            data: data,
        })
    },
    //删除产品
    deleteProduct(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/delete',
            method: 'delete',
            params: data
        })
    },
    // 获取用户列表
    getUserList(data) {
        return axios({
            url: '/sys/user/list',
            method: 'get',
            params: data,
        })
    },
    //查询用户
    queryUserId(data) {
        return axios({
            url: '/sys/user/queryById',
            method: 'get',
            params: data,
        })
    },
    //添加职位
    addpositionMgement(data) {
        return axios({
            url: '/enterprise/positionMgement/add',
            method: 'post',
            data: data,
        })
    },
    //修改职位
    updatepositionMgement(data) {
        return axios({
            url: '/enterprise/positionMgement/edit',
            method: 'put',
            data: data
        })
    },
    //岗位列表
    positionMgementList(data) {
        return axios({
            url: '/posdatapro/posDataPro/list',
            method: 'get',
            params: data,
        })
    },
    //查询已申请岗位简历
    queryByPostId(data) {
        return axios({
            url: '/apply/applyPosition/queryByPostId',
            method: 'get',
            params: data,
        })
    },
    //通过Id查询职位
    queryByMgementId(data) {
        return axios({
            url: '/enterprise/positionMgement/queryById',
            method: 'get',
            params: data,
        })
    },
    //通过Id查询宣讲会
    queryPreachId(data) {
        return axios({
            url: '/enterprise/preachMeeting/queryById',
            method: 'get',
            params: data,
        })
    },
    //分页查询职位
    pagePositionMgementList(data) {
        return axios({
            url: '/enterprise/positionMgement/list',
            method: 'get',
            params: data,
        })
    },
    //招聘会分页查询
    pageRecruitmentList(data) {
        return axios({
            url: '/recruitment/recruitmentFair/list',
            method: 'get',
            params: data,
        })
    },
    //删除招聘会
    deleteRecruit(data) {
        return axios({
            url: '/recruitment/recruitmentFair/delete',
            method: 'delete',
            params: data,
        })
    },
    //单位获取双选会
    pageDouElection(data) {
        return axios({
            url: '/doubleelection/douElection/list',
            method: 'get',
            params: data,
        })
    },
    //通过ID删除双选会
    deleteDoubleElection(data) {
        return axios({
            url: '/doubleelection/doubleElection/delete',
            method: 'delete',
            params: data,
        })
    },
    //删除宣讲会
    deletePreach(data) {
        return axios({
            url: '/enterprise/preachMeeting/delete',
            method: 'delete',
            params: data,
        })
    },
    //宣讲会列表
    pagePreachList(data) {
        return axios({
            url: '/enterprise/preachMeeting/list',
            method: 'get',
            params: data,
        })
    },
    //学校接口
    pageSchoolList(data) {
        return axios({
            url: '/school/schoolInformation/list',
            method: 'get',
            params: data,
        })
    },
    //通过id查询学校接口
    pageSchoolId(data) {
        return axios({
            url: '/school/schoolInformation/queryById',
            method: 'get',
            params: data,
        })
    },
    //场地主接口
    pageRootList(data) {
        return axios({
            url: '/site/siteClass/rootList',
            method: 'get',
            params: data,
        })
    },
    //场地子接口
    pageRootchildList(data) {
        return axios({
            url: '/site/siteClass/childList',
            method: 'get',
            params: data,
        })
    },
    //宣讲会发布
    preachMeetingAdd(data) {
        return axios({
            url: '/enterprise/preachMeeting/add',
            method: 'post',
            data: data,
        })
    },
    //宣讲会修改
    preachMeetingEdit(data) {
        return axios({
            url: '/enterprise/preachMeeting/edit',
            method: 'put',
            data: data
        })
    },
    //职位下架
    doPositionReovkeData(data) {
        return axios({
            url: '/enterprise/positionMgement/doPositionReovkeData',
            method: 'get',
            params: data
        })
    },
    //职位上架
    doPositionOnline(data) {
        return axios({
            url: '/enterprise/positionMgement/doPositionOnline',
            method: 'get',
            params: data
        })
    },
    //企业对学生实习评价-分页列表查询
    pageEvaluationList(data) {
        return axios({
            url: '/evaluate/internshipEvaluation/list',
            method: 'get',
            params: data,
        })
    },
    //企业对学生实习评价-添加
    evaluationAdd(data) {
        return axios({
            url: '/evaluate/internshipEvaluation/editEvaluation',
            method: 'post',
            data: data,
        })
    },
    //企业对学生实习评价-通过id查询
    queryByValuationId(data) {
        return axios({
            url: '/evaluate/internshipEvaluation/queryById',
            method: 'get',
            params: data,
        })
    },
    //企业对学生实习评价-通过id删除
    deleteEvaluation(data) {
        return axios({
            url: '/evaluate/internshipEvaluation/delete',
            method: 'delete',
            params: data,
        })
    },
    //实习签到-通过id删除
    signInfoDelete(data) {
        return axios({
            url: '/sign/signInfo/delete',
            method: 'delete',
            params: data,
        })
    },
    //实习签到分页列表查询
    pageSignList(data) {
        return axios({
            url: '/sign/signInfo/list',
            method: 'get',
            params: data,
        })
    },
    //请假管理-分页列表查询
    pageLeaveManagement(data) {
        return axios({
            url: '/leave/leaveManagement/list',
            method: 'get',
            params: data,
        })
    },
    //请假审批单个查询
    queryLeaveManagement(data) {
        return axios({
            url: '/leave/leaveManagement/queryById',
            method: 'get',
            params: data,
        })
    },
    //审批说明、
    leaveManagementCheck(data) {
        return axios({
            url: '/leave/leaveManagement/edit',
            method: 'put',
            data: data
        })
    },
    deleteLeaveManagement(data) {
        return axios({
            url: '/leave/leaveManagement/delete',
            method: 'delete',
            params: data,
        })
    },
    //简历中心表名分页中心
    pageListByIds(data) {
        return axios({
            url: '/enterprise/positionMgement/listByIds',
            method: 'get',
            params: data,
        })
    },
    //企业产品列表查询
    pageEnterProduct(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/list',
            method: 'get',
            params: data,
        })
    },
    //通过ID查询企业产品信息
    pageProductId(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/queryById',
            method: 'get',
            params: data,
        })
    },
    //编辑企业产品
    editProduct(data) {
        return axios({
            url: '/enterprise/enterpriseProducts/edit',
            method: 'put',
            data: data
        })
    },
    //新闻列表查询
    pageEnterNews(data) {
        return axios({
            url: '/enterprise/corporateNews/list',
            method: 'get',
            params: data,
        })
    },
    //通过ID查询新闻信息
    pageNewsId(data) {
        return axios({
            url: '/enterprise/corporateNews/queryById',
            method: 'get',
            params: data,
        })
    },
    //编辑企业新闻
    editNews(data) {
        return axios({
            url: '/enterprise/corporateNews/edit',
            method: 'put',
            data: data
        })
    },
    //企业福利
    pageUnitWelfare(data) {
        return axios({
            url: '/welfare/unitWelfare/list',
            method: 'get',
            params: data,
        })
    },
    //增加企业福利
    unitWelfareAdd(data) {
        return axios({
            url: '/welfare/unitWelfare/add',
            method: 'post',
            data: data,
        })
    },
    //删除福利
    deleteWelfareId(data) {
        return axios({
            url: '/welfare/unitWelfare/delete',
            method: 'delete',
            params: data,
        })
    },
    //企业入驻学校
    pageSchSettledin(data) {
        return axios({
            url: '/schooldatapro/entSchSettledin/list',
            method: 'get',
            params: data,
        })
    },
    //企业相册增加
    enter_priseAlbumAdd(data) {
        return axios({
            url: '/album/enterpriseAlbum/add',
            method: 'post',
            data: data,
        })
    },
    //企业相册修改
    edit_priseAlbum(data) {
        return axios({
            url: '/album/enterpriseAlbum/edit',
            method: 'put',
            data: data
        })
    },
    //企业相册查询
    pagePriseList(data) {
        return axios({
            url: '/album/enterpriseAlbum/list',
            method: 'get',
            params: data,
        })
    },
    //企业相册单个ID查询
    queryPriseId(data) {
        return axios({
            url: '/album/enterpriseAlbum/queryById',
            method: 'get',
            params: data,
        })
    },
    //企业相册ID删除
    deletePriseId(data) {
        return axios({
            url: '/album/enterpriseAlbum/delete',
            method: 'delete',
            params: data,
        })
    },
    //岗位中间表ID查询
    queryApplyId(data) {
        return axios({
            url: '/apply/applyPosition/queryById',
            method: 'get',
            params: data,
        })
    },
    //分页查询学校审核职位列表
    querySchoolPosition(data) {
        return axios({
            url: '/posdatapro/schoolPositionToexamine/list',
            method: 'get',
            params: data,
        })
    },

    //职位字典
    dictPostType(data) {
        return axios({
            url: '/dict/dictPostType/list',
            method: 'get',
            params: data,
        })
    },
    //行业字典
    dictUnitTrade(data) {
        return axios({
            url: '/dict/dictUnitTrade/list',
            method: 'get',
            params: data,
        })
    },
    //实习日志
    enter_studentReport(data) {
        return axios({
            url: '/report/studentReport/list',
            method: 'get',
            params: data,
        })
    },
    //生源信息
    enter_SourceInfo(data) {
        return axios({
            url: '/source/sourceInfo/list',
            method: 'get',    //企业入驻学校
            params: data,
        })
    },
    edit_entSchSettledin(data) {
        return axios({
            url: '/schooldatapro/entSchSettledin/edit',
            method: 'put',
            data: data
        })
    },

    //企业基本信息完整度
    enter_priseInformationView(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/EnterpriseInformationView',
            method: 'get',
            params: data,
        })
    },
    //通过生源ID查询信息
    enter_SourceInfoId(data) {
        return axios({
            url: '/source/sourceInfo/queryById',
            method: 'get',
            params: data,
        })
    },
    //删除日志
    deleteStudentReport(data) {
        return axios({
            url: '/report/studentReport/delete',
            method: 'delete',
            params: data,
        })
    },
    //职务管理
    enter_position(data) {
        return axios({
            url: '/sys/position/list',
            method: 'get',
            params: data,
        })
    },
    //增加
    enter_positionAdd(data) {
        return axios({
            url: '/sys/position/add',
            method: 'post',
            data: data,
        })
    },
    //修改
    edit_position(data) {
        return axios({
            url: '/sys/position/edit',
            method: 'put',
            data: data
        })
    },
    //删除
    delete_position(data) {
        return axios({
            url: '/sys/position/delete',
            method: 'delete',
            params: data,
        })
    },
    //批量删除
    deleteBatch_position(data) {
        return axios({
            url: '/sys/position/deleteBatch',
            method: 'delete',
            params: data,
        })
    },
    //企业（人才推荐）
    queryTalent(data) {
        return axios({
            url: '/apply/applyPosition/queryTalent',
            method: 'get',
            params: data,
        })
    },
    //平台通知
    queryListAnt(data) {
        return axios({
            url: '/itoSystem/announcement/listAnt',
            method: 'get',
            params: data,
        })
    },
    //薪资字典
    querydictSalary(data) {
        return axios({
            url: '/dict/dictSalary/list',
            method: 'get',
            params: data,
        })
    },
    // 获取学校信息
    getSchoolList(data) {
        return axios({
            url: '/school/schoolInformation/list',
            method: 'get',
            params: data,
        })
    },
    // 企业、学校入驻中间表
    addSettledin(data) {
        return axios({
            url: '/schen/schEnterBetween/add',
            method: 'post',
            data: data
        })
    },
    // 企业是否入驻修改方法
    editcompanyset(data) {
        return axios({
            url: '/schen/schEnterBetween/edit',
            method: 'put',
            data: data
        })
    },
    // 查询企业是否入驻
    querycompanyset(data) {
        return axios({
            url: '/schen/schEnterBetween/list',
            method: 'get',
            params: data,
        })
    },
    // 取消入驻即删除
    deletecompantset(data) {
        return axios({
            url: '/schen/schEnterBetween/delete',
            method: 'delete',
            params: data,
        })
    },
    // 单位性质字典表
    getunitCode(data){
        return axios({
            url: '/dict/dictUnitAttribute/list',
            method: 'get',
            params: data,
        })
    }
}

export default enter
