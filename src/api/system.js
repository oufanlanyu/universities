import Vue from 'vue'
import { axios } from '@/utils/request'

const system = {
    //角色维护
     enter_SysRoleUser(data){
        return axios({
            url: '/sys/role/list',
            method: 'get',
            params: data,
        })
    },
    //增加
    enter_SysRoleAdd(data){
      return axios({
            url: '/sys/role/add',
            method: 'post',
            data: data,
      })
    },
    //修改
    edit_SysRole(data){
        return axios({
            url: '/sys/role/edit',
            method: 'put',
            data: data
        })
    },
    //删除
    delete_RoleUser(data){
      return axios({
            url: '/sys/role/delete',
            method: 'delete',
            params: data,
        })
    },
    //用户管理
    sys_UserList(data){
        return axios({
            url: '/sys/user/list',
            method: 'get',
            params: data,
        })
    },

    //增加用户
    sys_UserAdd(data){
        return axios({
            url: '/sys/user/add',
            method: 'post',
            data: data,
        })
    },
    //修改用户
    sys_UserEdit(data){
        return axios({
            url: '/sys/user/edit',
            method: 'put',
            data: data,
        })
    },
    //删除用户
    delete_User(data){
        return axios({
            url: '/sys/user/delete',
            method: 'delete',
            params: data,
        })
    },
    //选择部门
    queryIdTree(data){
        return axios({
            url: '/sys/sysDepart/queryIdTree',
            method: 'get',
            params: data,
        })
    },
    // 通过id获取用户表的数据
    getuserqueryById(data){
        return axios({
            url: '/sys/user/queryById',
            method: 'get',
            params: data,
        })
    },
    //用户权限
    queryUserRole(data){
        return axios({
            url: '/sys/user/queryUserRole',
            method: 'get',
            params: data,
        })
    },
    //通过id查找部门权限
    queryUserDepart(data){
        return axios({
            url: '/sys/user/userDepartList',
            method: 'get',
            params: data,
        })
    },
    //冻结账户
    sys_frozenBatch(data){
        return axios({
            url: '/sys/user/frozenBatch',
            method: 'put',
            data: data,
        })
    },
    //当前角色权限
    queryRolePermission(data){
        return axios({
            url: '/sys/permission/queryRolePermission',
            method: 'get',
            params: data,
        })
    },
    //权限树
    queryTreeList(data){
        return axios({
            url: '/sys/role/queryTreeList',
            method: 'get',
            params: data,
        })
    },
    //授权保存
    saveRolePermission(data){
        return axios({
            url: '/sys/permission/saveRolePermission',
            method: 'post',
            data: data,
        })
    },
    //系统消息
    getMyAnnouncementSend(data){
        return axios({
            url: '/sys/sysAnnouncementSend/getMyAnnouncementSend',
            method: 'get',
            params: data,
        })
    },
    //系统消息读取状态
    editByAnntIdAndUserId(data){
        return axios({
            url: '/sys/sysAnnouncementSend/editByAnntIdAndUserId',
            method: 'put',
            data: data,
        })
    },
    //部门管理Tree
    departqueryTreeList(data){
        return axios({
            url: '/sys/sysDepart/queryTreeList',
            method: 'get',
            params: data,
        })
    },
    //添加部门
    sysDepartAdd(data){
      return axios({
            url: '/sys/sysDepart/add',
            method: 'post',
            data: data,
      })
    },
    //修改部门
    sysDepartEdit(data){
      return axios({
            url: '/sys/sysDepart/edit',
            method: 'put',
            data: data,
      })
    },
    //删除部门
    sysDepartDeleteBatch(data){
        return axios({
            url: '/sys/sysDepart/deleteBatch',
            method: 'delete',
            params: data,
        })
    },
    //部门用户
    departUserList(data){
        return axios({
            url: ' /sys/user/departUserList',
            method: 'get',
            params: data,
        })
    }
}

export default system
