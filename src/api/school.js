import Vue from 'vue'
import { axios } from '@/utils/request'

let school = {
	// 获取文章分类列表
	getArtCatgory(data){
		return axios({
			url: '/news/contentCategory/rootList',
			method: 'get',
			params: data,
		})
	},
	// 获取文章子分类
	getArtCatgoryChild(data){
		return axios({
			url: '/news/contentCategory/childList',
			method: 'get',
			params: data,
		})
	},
	// 通过id查询文章分类
	getArticlequeryByid(data){
		return axios({
			url: '/news/contentCategory/queryById',
			method: 'get',
			params: data,
		})
	},
	// 获取文章列表
	getArticleList(data){
		return axios({
			url: '/news/newsInformation/list',
			method: 'get',
			params: data,
		})
	},
	// 通过id查询文章详情
	getArticleDetail(data){
		return axios({
			url: '/news/newsInformation/queryById',
			method: 'get',
			params: data,
		})
	},
	// 获取招聘会列表
	getInSchoolfair(data) {
		return axios({
			url: '/recruitment/recruitmentFair/list',
			method: 'get',
			params: data,
		})
	},
	// 根据id获取招聘会详情
	getISFairqueryById(data) {
		return axios({
			url: '/recruitment/recruitmentFair/queryById',
			method: 'get',
			params: data,
		})
	},
	// 获取宣讲会列表
	getPreachmeeting(data){
		return axios({
			url: '/enterprise/preachMeeting/list',
			method: 'get',
			params: data,
		})
	},
	// 根据id获取宣讲会详情
	getPreachmeetqueryById(data) {
		return axios({
			url: '/enterprise/preachMeeting/queryById',
			method: 'get',
			params: data,
		})
	},
	// 申请岗位
	applyPost(data) {
		return axios({
			url: '/apply/applyPosition/addApply',
			method: 'post',
			params: data,
		})
    },
    // 收藏岗位
    collectPost(data){
        return axios({
			url: '/apply/applyPosition/addCollect',
			method: 'post',
			params: data,
		})
    },
	// 申请职位前置查询，判断用户是否已申请
	getStuidqueryById(data) {
		return axios({
			url: '/apply/applyPosition/list',
			method: 'get',
			params: data,
		})
    },
    // 关注单位list查询
    unitofconlist(data){
        return axios({
			url: '/follow/followEnterprise/list',
			method: 'get',
			params: data,
		})
    },
    // 关注单位
    unitofconcern(data){
        return axios({
			url: '/follow/followEnterprise/add',
			method: 'post',
			data: data,
		})
    },
    // 岗位列表list分页查询
    getOldPostList(data){
        return axios({
			url: '/enterprise/positionMgement/list',
			method: 'get',
			params: data,
		})
    },
	// 获取岗位列表
	getPostList(data) {
		return axios({
			url: '/enterprise/positionMgement/queryPositionList',
			method: 'get',
			params: data,
		})
	},
	// 根据岗位id获取详情
	getPostDetail(data) {
		return axios({
			url: '/enterprise/positionMgement/queryEnterInfoByPostId',
			method: 'get',
			params: data,
		})
    },
	// 根据顶级场地获取场地信息
	getVenue(data) {
		return axios({
			url: '/site/siteClass/childList',
			method: 'get',
			params: data,
		})
	},
	// 修改场地
	editVenue(data) {
		return axios({
			url: '/site/siteClass/edit',
			method: 'put',
			data: data,
		})
	},
	// 招聘会报名中间表查询
	getRecruitcenter(data){
		return axios({
			url: '/recruitment/registrationStatistics/list',
			method: 'get',
			params: data,
		})
	},
	// 招聘会报名
	addSignupFairS(data) {
		return axios({
			url: '/recruitment/registrationStatistics/add',
			method: 'post',
			data: data,
		})
	},
	// 企业基本信息列表查询
	getCompanyinfo(data){
		return axios({
			url: '/enterprise/enterpriseInformation/list',
			method: 'get',
			params: data,
		})
	},
	// 获取学校基本信息
	getSchoolInfo(data){
		return axios({
			url: '/school/schoolInformation/list',
			method: 'get',
			params: data,
		})
	},
	// 获取企业基本信息
	getcompanyqueryById(data){
		return axios({
			url: '/enterprise/enterpriseInformation/queryById',
			method: 'get',
			params: data,
		})
    },
    // 获取单位相册
    getcommaxbo(data){
        return axios({
			url: '/album/enterpriseAlbum/list',
			method: 'get',
			params: data,
		})
    },
    // 获取单位新闻通过id
    getcompanyNewsqueryById(data){
        return axios({
			url: '/enterprise/corporateNews/queryById',
			method: 'get',
			params: data,
		})
    },
	// 获取单位新闻列表
	getcompanyNewsList(data){
		return axios({
			url: '/enterprise/corporateNews/list',
			method: 'get',
			params: data,
		})
    },
    // 获取单位产品列表通过id
    getcompanyproductqueryById(data){
        return axios({
			url: '/enterprise/enterpriseProducts/queryById',
			method: 'get',
			params: data,
		})
    },
	// 获取单位产品列表
	getcompanyProductList(data){
		return axios({
			url: '/enterprise/enterpriseProducts/list',
			method: 'get',
			params: data,
		})
	},
	// 获取培训会列表
	gettrainingList(data){
		return axios({
			url: '/innovate/trainingCourseMent/list',
			method: 'get',
			params: data,
		})
	},
	// 获取培训会详情
	gettrainingqueryById(data){
		return axios({
			url: '/innovate/trainingCourseMent/queryById',
			method: 'get',
			params: data,
		})
	},
	// 培训会报名
	trainSignUp(data){
		return axios({
			url: '/trainingsignup/trainingSignup/add',
			method: 'post',
			data: data,
		})
	},
	// 培训会报名表查询
	trainSignupList(data){
		return axios({
			url: '/trainingsignup/trainingSignup/list',
			method: 'get',
			params: data,
		})
	},
	// 校外招聘会/事业单位招聘列表
	getOffPublicList(data){
		return axios({
			url: '/recruitment/institutionRecruitment/list',
			method: 'get',
			params: data,
		})
	},
	// 校外招聘会/事业单位招聘querybyid查询
	offPublicqueryById(data){
		return axios({
			url: '/recruitment/institutionRecruitment/queryById',
			method: 'get',
			params: data,
		})
	},
	// 轮播图
	getWheelList(data){
		return axios({
			url: '/carousel/carouselMap/list',
			method: 'get',
			params: data,
		})
	},
	// 友情链接
	getFriendsLinkList(data){
		return axios({
			url: '/friendly/friendLinks/list',
			method: 'get',
			params: data,
		})
	},
	// 单位入驻学校
	settlementunitList(data){
		return axios({
			url: '/enterprise/enterpriseInformation/list',
			method: 'get',
			params: data,
		})
    },
    // 获取院系风采
    getschooldepartList(data){
        return axios({
			url: '/school/schoolDepart/list',
			method: 'get',
			params: data,
		})
    },
    // 根据id获取院系风采
    getdepartqueryById(data){
        return axios({
			url: '/school/schoolDepart/queryById',
			method: 'get',
			params: data,
		})
    },
    // 获取单位福利待遇
    getwelfarelist(data){
        return axios({
			url: '/welfare/unitWelfare/list',
			method: 'get',
			params: data,
		})
    },
    // 宣讲会报名
    preachmentsign(data){
        return axios({
			url: '/preaching/preachingSignup/add',
			method: 'post',
			data: data,
		})
    },
    // 宣讲会报名list查询
    preachmentlist(data){
        return axios({
			url: '/preaching/preachingSignup/list',
			method: 'get',
			params: data,
		})
    },
    // 工作经验字典
    getWorkhandsList(data){
        return axios({
			url: '/dict/dictWorkExperience/list',
			method: 'get',
			params: data,
		})
    },
    // 薪资字典
    getSalaryList(data){
        return axios({
			url: '/dict/dictSalary/list',
			method: 'get',
			params: data,
		})
    },
    // 学历字典
    getEdurankList(data){
        return axios({
			url: '/dict/dictEduRank/list',
			method: 'get',
			params: data,
		})
    },
    // 从事行业
    getUnitTradeList(data){
        return axios({
			url: '/dict/dictUnitTrade/list',
			method: 'get',
			params: data,
		})
    },
    // 福利字典
    getWelfareList(data){
        return axios({
			url: '/dict/dictEnterWelfare/list',
			method: 'get',
			params: data,
		})
    },
    // 单位性质字典
    getPropertyList(data){
        return axios({
			url: '/dict/dictUnitAttribute/list',
			method: 'get',
			params: data,
		})
    },
    // 单位规模字典
    getEnterScaleList(data){
        return axios({
			url: '/dict/dictEnterScale/list',
			method: 'get',
			params: data,
		})
    },
}

export default school
