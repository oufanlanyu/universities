import Vue from 'vue'
import { axios } from '@/utils/request'

const company = {
    // 企业基本信息list查询
    getcompanylist(data){
        return axios({
            url: '/enterprise/enterpriseInformation/list',
            method: 'get',
            params: data,
        })
    },
    // 通过服务商id查询企业基本信息
    getcompanytenantId(data){
        return axios({
            url: '/enterprise/enterpriseInformation/queryByTenantId',
            method: 'get',
            params: data,
        })
    },
    // 添加企业基本信息
    addcompanyinfo(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/add',
            method: 'post',
            data: data
        })
    },
    //修改企业基本信息
    enitenterinfo(data) {
        return axios({
            url: '/enterprise/enterpriseInformation/edit',
            method: 'put',
            data: data
        })
    },
    // 企业基本信息完整度
    comanyIntegrity(data){
        return axios({
            url: '/enterprise/enterpriseInformation/queryEnCountByTenantIdId',
            method: 'get',
            params: data,
        })
    },
    // 从事行业字典
    getUnitTradeList(data){
        return axios({
			url: '/dict/dictUnitTrade/list',
			method: 'get',
			params: data,
		})
    },
    // 单位性质字典
    getPropertyList(data){
        return axios({
			url: '/dict/dictUnitAttribute/list',
			method: 'get',
			params: data,
		})
    },
    // 单位规模字典
    getEnterScaleList(data){
        return axios({
			url: '/dict/dictEnterScale/list',
			method: 'get',
			params: data,
		})
    },
    // 福利字典
    getWelfareList(data){
        return axios({
			url: '/dict/dictEnterWelfare/list',
			method: 'get',
			params: data,
		})
    },
    // 薪资字典
    getSalaryList(data){
        return axios({
			url: '/dict/dictSalary/list',
			method: 'get',
			params: data,
		})
    },
    // 工作经验字典
    getWorkhandsList(data){
        return axios({
			url: '/dict/dictWorkExperience/list',
			method: 'get',
			params: data,
		})
    },
    // 学历字典
    getEdurankList(data){
        return axios({
			url: '/dict/dictEduRank/list',
			method: 'get',
			params: data,
		})
    },
    // 添加单位相册
    Addalbum(data) {
        return axios({
            url: '/album/enterpriseAlbum/add',
            method: 'post',
            data: data,
        })
    },
    // 获取单位相册列表
    getAlbumList(data){
        return axios({
            url: '/album/enterpriseAlbum/list',
            method: 'get',
            params: data,
        })
    },
    // 删除单位相册
    deleteAlbum(data) {
        return axios({
            url: '/album/enterpriseAlbum/delete',
            method: 'delete',
            params: data,
        })
    },
    // 添加单位产品
    addProducts(data){
        return axios({
            url: '/enterprise/enterpriseProducts/add',
            method: 'post',
            data: data,
        })
    },
    // 通过id查询单位产品
    getProductqueryById(data){
        return axios({
            url: '/enterprise/enterpriseProducts/queryById',
            method: 'get',
            params: data,
        })
    },
    // 修改单位产品
    EditProduct(data){
        return axios({
            url: '/enterprise/enterpriseProducts/edit',
            method: 'put',
            data: data
        })
    },
    // 添加单位新闻
    AddComNews(data) {
        return axios({
            url: '/enterprise/corporateNews/add',
            method: 'post',
            data: data,
        })
    },
    // 通过id查询单位新闻
    getNewsqueryById(data){
        return axios({
            url: '/enterprise/corporateNews/queryById',
            method: 'get',
            params: data,
        })
    },
    // 修改单位产新闻
    EditComNews(data){
        return axios({
            url: '/enterprise/corporateNews/edit',
            method: 'put',
            data: data
        })
    },
    // 获取学校基本信息列表
    GetSchoolList(data){
        return axios({
            url: '/school/schoolInformation/list',
            method: 'get',
            params: data,
        })
    },
    // 获取场地列表
    GetpageRootList(data) {
        return axios({
            url: '/site/siteClass/rootList',
            method: 'get',
            params: data,
        })
    },
    // 获取场地子数据
    GetfieldChildList(data) {
        return axios({
            url: '/site/siteClass/childList',
            method: 'get',
            params: data,
        })
    },
    // 发送邮箱验证码
	sendmailcode(data){
		return axios({
			url: '/sys/user/getEmailCode',
			method: 'post',
			data: data
		})
	},
	// 邮箱验证
	emailverif(data){
		return axios({
			url: '/sys/user/checkEmailCode',
			method: 'post',
			data: data
		})
    },
    // 用户信息修改
	setuserinfo(data) {
		return axios({
			url: '/sys/user/edit',
			method: 'put',
			data: data
		})
	},
}

export default company
