import Vue from 'vue'
import { axios } from '@/utils/request'

const user = {
	// 获取服务器当前时间
	getServerTime(data){
		return axios({
            url: '/systime/sysTime/getTime',
            method: 'get',
            params: data,
        })
	},
	// 通过id获取用户表的数据
	getuserqueryById(data){
		return axios({
            url: '/sys/user/queryById',
            method: 'get',
            params: data,
        })
	},
	// 获取用户表的数据list
    getuserinfo(data) {
        return axios({
            url: '/sys/user/list',
            method: 'get',
            params: data,
        })
	},
	// 用户信息修改
	setuserinfo(data) {
		return axios({
			url: '/sys/user/edit',
			method: 'put',
			data: data
		})
	},
    // 通过sourceId获取生源信息
    getsourceinfo(data) {
		return axios({
			url: '/source/sourceInfo/queryById',
			method: 'get',
			params: data,
		})
	},
	// 生源信息修改
	setsourceinfo(data) {
		return axios({
			url: '/source/sourceInfo/edit',
			method: 'put',
			data: data
		})
	},
	// 文件上传接口
	uploadFile() {
		return axios({
			url: '/sys/common/upload',
			method: 'post',
			data: data
		})
	},
	// 个人简历查询通过简历Id
    getresumequeryById(data) {
		return axios({
			url: '/resume/resumeInfo/queryById',
			method: 'get',
			params: data,
		})
    },
    // 个人简历查询通过SourceId
    getresumeinfo(data) {
		return axios({
			url: '/resume/resumeInfo/queryBySourceId',
			method: 'get',
			params: data,
		})
    },
    // 个人简历修改
    setresumeinfo(data) {
		return axios({
			url: '/resume/resumeInfo/finish',
			method: 'put',
			data: data
		})
    },
    // 学生申请岗位列表
    getStuPostList(data){
        return axios({
			url: '/enterprise/positionMgement/getMyApplyPost',
			method: 'get',
			params: data,
		})
    },
    // 学生面试通知列表
    getStuInvList(data){
        return axios({
			url: '/enterprise/positionMgement/getMyInterviewNotifyList',
			method: 'get',
			params: data,
		})
    },
	// 我的岗位中间表
	getMyPostcenter(data) {
		return axios({
			url: '/enterprise/positionMgement/queryMyPostList',
			method: 'get',
			params: data,
		})
	},
	// 申请岗位修改
	getMyPostqueryById(data){
		return axios({
			url: '/apply/applyPosition/edit',
			method: 'put',
			data: data,
		})
	},
	// 取消申请岗位
	deleteWithdraw(data){
		return axios({
			url: '/apply/applyPosition/delete',
			method: 'delete',
			params: data,
		})
    },
    // 拒绝面试
    refuseinter(data){
        return axios({
			url: '/apply/applyPosition/refuseInvitation',
			method: 'put',
			data: data,
		})
    },
	// 传入表名和id批量查询
	getTableNameBatch(data){
		return axios({
			url: '/enterprise/positionMgement/listByIds',
			method: 'get',
			params: data,
		})
	},
	// 我的职位通过id查询
	getMyPostqueryId(data) {
		return axios({
			url: '/enterprise/positionMgement/queryById',
			method: 'get',
			params: data,
		})
    },
    // 岗位列表list分页查询
    getOldPostList(data){
        return axios({
			url: '/enterprise/positionMgement/list',
			method: 'get',
			params: data,
		})
    },
	// 实习日志列表
	getInternshiplist(data) {
		return axios({
			url: '/report/studentReport/list',
			method: 'get',
			params: data,
		})
	},
	// 实习日志通过id查询
	setInternshipday(data) {
		return axios({
			url: '/report/studentReport/queryById',
			method: 'get',
			params: data,
		})
	},
	// 编辑实习日志
	setInternshipone(data) {
		return axios({
			url: '/report/studentReport/edit',
			method: 'put',
			data: data
		})
	},
	// 添加实习日志
	addInternshipday(data) {
		return axios({
			url: '/report/studentReport/add',
			method: 'post',
			data: data
		})
	},
	// 删除实习日志
	deleteInternshipday(data) {
		return axios({
			url: '/report/studentReport/delete',
			method: 'delete',
			params: data,
		})
	},
	// 获取请假申请列表
	getApplyLeave(data) {
		return axios({
			url: '/leave/leaveManagement/list',
			method: 'get',
			params: data,
		})
	},
	// 添加请假申请
	addApplyLeave(data) {
		return axios({
			url: '/leave/leaveManagement/add',
			method: 'post',
			data: data
		})
	},
	// 请假申请通过id查询
	getqueryIdLeave(data) {
		return axios({
			url: '/leave/leaveManagement/queryById',
			method: 'get',
			params: data,
		})
	},
	// 编辑请假申请
	setApplyLeave(data) {
		return axios({
			url: '/leave/leaveManagement/edit',
			method: 'put',
			data: data
		})
	},
	// 删除请假申请
	deleteApplyLeave(data) {
		return axios({
			url: '/leave/leaveManagement/delete',
			method: 'delete',
			params: data,
		})
	},
	// 获取老师发布的实习作业
	getteacherpractice(data) {
		return axios({
			url: '/task/practiceTask/list',
			method: 'get',
			params: data,
		})
	},
	// 获取实习作业列表
	getOperation(data) {
		return axios({
			url: '/task/submitTask/list',
			method: 'get',
			params: data,
		})
	},
	// 实习作业通过id查询
	getqueryIdOperation(data) {
		return axios({
			url: '/task/submitTask/queryById',
			method: 'get',
			params: data,
		})
	},
	// 添加实习作业
	addOperation(data) {
		return axios({
			url: '/task/submitTask/add',
			method: 'post',
			data: data
		})
	},
	// 编辑实习作业
	setOperation(data) {
		return axios({
			url: '/task/submitTask/edit',
			method: 'put',
			data: data
		})
	},
	// 删除实习作业
	deleteOperation(data) {
		return axios({
			url: '/task/submitTask/delete',
			method: 'delete',
			params: data,
		})
	},
	// 获取自主实习列表
	getAutopractice(data) {
		return axios({
			url: '/apply/practiceApply/list',
			method: 'get',
			params: data,
		})
	},
	// 添加自主实习
	addAutopractice(data) {
		return axios({
			url: '/apply/practiceApply/add',
			method: 'post',
			data: data
		})
	},
	// 删除自主实习
	deleteAutopractice(data) {
		return axios({
			url: '/apply/practiceApply/delete',
			method: 'delete',
			params: data,
		})
	},
	// 获取三方协议列表
	getThreeparty(data) {
		return axios({
			url: '/agreement/uploadAgreement/list',
			method: 'get',
			params: data,
		})
	},
	// 三方协议通过id查询
	getqueryIdThreeparty(data) {
		return axios({
			url: '/agreement/uploadAgreement/queryById',
			method: 'get',
			params: data,
		})
	},
	// 添加三方协议
	addThreeparty(data) {
		return axios({
			url: '/agreement/uploadAgreement/add',
			method: 'post',
			data: data
		})
	},
	// 编辑三方协议
	editThreeparty(data) {
		return axios({
			url: '/agreement/uploadAgreement/edit',
			method: 'put',
			data: data
		})
	},
	// 删除三方协议
	deleteThreeparty(data) {
		return axios({
			url: '/agreement/uploadAgreement/delete',
			method: 'delete',
			params: data,
		})
	},
	// 职位管理列表
	getPostlist(data){
		return axios({
			url: '/enterprise/positionManagement/list',
			method: 'get',
			params: data,
		})
	},
	// 修改密码
	editPassword(data){
		return axios({
			url: '/sys/user/updatePassword',
			method: 'put',
			data: data
		})
	},
	// 简历完整度
	getresumeIntegrity(data){
		return axios({
			url: '/resume/resumeInfo/queryCountById',
			method: 'get',
			params: data,
		})
	},
	// 个人中心为你优选
	getOptimization(data){
		return axios({
			url: '/apply/applyPosition/goodJobList',
			method: 'get',
			params: data,
		})
	},
	// 培训会报名表查询
	trainSignupList(data){
		return axios({
			url: '/trainingsignup/trainingSignup/list',
			method: 'get',
			params: data,
		})
	},
	// 培训会报名编辑
	editTrainSingup(data){
		return axios({
			url: '/trainingsignup/trainingSignup/edit',
			method: 'put',
			data: data
		})
	},
	// 培训会报名表删除
	deletetrainSignup(data){
		return axios({
			url: '/trainingsignup/trainingSignup/delete',
			method: 'delete',
			params: data,
		})
	},
	// 简历外发
	addresumeExternal(data){
		return axios({
			url: '/external/resumeExternal/add',
			method: 'POST',
			data: data
		})
	},
	// 简历外发列表
	getresumelist(data){
		return axios({
			url: '/external/resumeExternal/list',
			method: 'get',
			params: data,
		})
	},
	// 简历外发删除
	deleteresumesignup(data){
		return axios({
			url: '/external/resumeExternal/delete',
			method: 'delete',
			params: data,
		})
	},
	// 获取招聘会列表
	getInSchoolfair(data) {
		return axios({
			url: '/recruitment/recruitmentFair/list',
			method: 'get',
			params: data,
		})
	},
	// 招聘会报名表列表
	getjobfaiRegistList(data){
		return axios({
			url: '/recruitment/registrationStatistics/list',
			method: 'get',
			params: data,
		})
	},
	// 取消报名招聘会
	deletejobfairRegist(data){
		return axios({
			url: '/recruitment/registrationStatistics/delete',
			method: 'delete',
			params: data,
		})
	},
	// 发送邮箱验证码
	sendmailcode(data){
		return axios({
			url: '/sys/user/getEmailCode',
			method: 'post',
			data: data
		})
	},
	// 邮箱验证
	emailverif(data){
		return axios({
			url: '/sys/user/checkEmailCode',
			method: 'post',
			data: data
		})
	},
	// 获取单位列表
	getcompanyList(data){
		return axios({
			url: '/enterprise/enterpriseInformation/list',
			method: 'get',
			params: data,
		})
	},
	// 学生屏蔽单位
	shieldEnterprises(data){
		return axios({
			url: '/shield/shieldEnter/add',
			method: 'post',
			data: data
		})
	},
	// 查询学生是否已屏蔽该企业
	getshieldlist(data){
		return axios({
			url: '/shield/shieldEnter/list',
			method: 'get',
			params: data,
		})
	},
	// 根据企业名称模糊查询
	fuzzyQuerycompany(data){
		return axios({
			url: '/enterprise/enterpriseInformation/list',
			method: 'get',
			params: data,
		})
	},
	// 取消屏蔽
	deleteshiedcompany(data){
		return axios({
			url: '/shield/shieldEnter/delete',
			method: 'delete',
			params: data,
		})
	},
	// 批量取消屏蔽
	deletebatchshied(data){
		return axios({
			url: '/shield/shieldEnter/deleteBatch',
			method: 'delete',
			params: data,
		})
	},
	// 实习群组
	getInternshipgroup(data){
		return axios({
			url: '/group/internshipGroup/list',
			method: 'get',
			params: data,
		})
    },
    // 实习安排
    getInternshiparrange(data){
        return axios({
			url: '/practice/practiceArrange/list',
			method: 'get',
			params: data,
		})
    },
    // 根据id获取实习计划
    getplanquerybyId(data){
        return axios({
			url: '/practice/practiceNotice/queryById',
			method: 'get',
			params: data,
		})
    },
    // 单位基本信息
    getcompantinfo(data){
        return axios({
			url: '/enterprise/enterpriseInformation/list',
			method: 'get',
			params: data,
		})
    },
    // 关注企业列表
    followunitlist(data){
        return axios({
			url: '/follow/followEnterprise/list',
			method: 'get',
			params: data,
		})
    },
    // 取消关注
    deleteFollow(data){
        return axios({
			url: '/follow/followEnterprise/delete',
			method: 'delete',
			params: data,
		})
    },
    // 获取宣讲会列表
	getPreachmeeting(data){
		return axios({
			url: '/enterprise/preachMeeting/list',
			method: 'get',
			params: data,
		})
	},
    // 宣讲会报名查询
    preachmentlist(data){
        return axios({
			url: '/enterprise/preachMeeting/getPreachList',
			method: 'get',
			params: data,
		})
    },
    // 宣讲会取消报名
    deletepreachment(data){
        return axios({
			url: '/preaching/preachingSignup/delete',
			method: 'delete',
			params: data,
		})
    },
    // 获取消息通知
    getMessage(data){
        return axios({
			url: '/sys/sysAnnouncementSend/getMyAnnouncementSend',
			method: 'get',
			params: data,
		})
    },
    // 消息已读状态
    editMessage(data){
        return axios({
			url: '/sys/sysAnnouncementSend/editByAnntIdAndUserId',
			method: 'put',
			data: data
		})
    },
    // 获取院系风采
    getschooldepartList(data){
        return axios({
			url: '/school/schoolDepart/list',
			method: 'get',
			params: data,
		})
    },
    // 从事行业
    getUnitTradeList(data){
        return axios({
			url: '/dict/dictUnitTrade/list',
			method: 'get',
			params: data,
		})
    },
    // 薪资字典
    getSalaryList(data){
        return axios({
			url: '/dict/dictSalary/list',
			method: 'get',
			params: data,
		})
    },
}

export default user
