import Vue from 'vue'
import api from '@/api/company.js'
import { Message } from 'element-ui'
import Pagi from "@/views/company/components/Pagi"
import { getAction } from '@/api/manage'

export const LxListMixin = {
    components: {
        Pagi
    },
    created() {
        this.loadDataTable()
        // 获取单位基本信息
        this.getcompanyinfo()
    },
    data() {
        return {
            // 服务商id
            tenantId: this.$store.state.tenantId,
            // 企业基本信息
            cominfo: {},
            // 表格数据
            tableData: [],
            /* table加载状态 */
			loading: false,
            // 分页参数
            pagi: {
                total: 0,
                current: 1,
                size: 10
            },
            /* 查询参数 */
            queryParam: {},
            /* 排序参数 */
			isorter: {
				column: 'createTime',
				order: 'desc',
            },
            /* 初始的查询筛选参数 */
            loadingParam: {},
            /* 筛选参数 */
			filters: {},
        }
    },
    methods: {
        // 加载数据
        loadDataTable(arg){
            if (!this.url.list) {
                Message.error('请设置url.list属性!')
				return
			}
            //加载数据 若传入参数1则加载第一页的内容
			if (arg === 1) {
				this.pagi.current = 1;
            }
            var params = this.getQueryParams();//查询条件
            this.loading = true;
			getAction(this.url.list, params).then((res) => {
				if (res.success) {
					this.tableData = res.result.records;
					this.pagi.total = res.result.total
                    this.pagi.current = res.result.current
                    this.pagi.size = res.result.size
				}
				this.loading = false;
			})
        },
        // 查询条件
        getQueryParams() {
			//获取查询条件
			var param = Object.assign(this.queryParam, this.isorter, this.filters, this.loadingParam);
			param.pageNo = this.pagi.current;
			param.pageSize = this.pagi.size;
			return this.filterObj(param);
        },
        /**
         * 过滤对象中为空的属性
         * @param obj
         * @returns {*}
         */
        filterObj(obj) {
            if (!(typeof obj == 'object')) {
              return;
            }

            for ( var key in obj) {
              if (obj.hasOwnProperty(key)
                && (obj[key] == null || obj[key] == undefined || obj[key] === '')) {
                delete obj[key];
              }
            }
            return obj;
        },
        // 获取单位基本信息
        getcompanyinfo(){
            let _this = this
            let tenantId = _this.tenantId
            api.getcompanytenantId({
                tenantId: _this.tenantId
            }).then(res => {
                if(res.success){
                    _this.cominfo = res.result
                    _this.verifauditStatus()
                }else{
                    _this.$router.push('/perfectinfo')
                }
            })
        },
        // 验证单位是否已经通过审核
        verifauditStatus(){
            let _this = this
            if(_this.cominfo.auditStatus == 2){
                if(!localStorage.getItem('auditpass')){
                    this.$notify({
                        title: '审核通过',
                        message: '资料审核通过!',
                        type: 'success'
                    })
                }
                localStorage.setItem('auditpass',true)
            }else if(_this.cominfo.auditStatus == 1){
                _this.$router.push('/perfectinfo')
                localStorage.removeItem('auditpass')
            }else if(_this.cominfo.auditStatus == 3){
                _this.$router.push('/perfectinfo')
                localStorage.removeItem('auditpass')
            }
        },
        /* 图片预览 */
		getImgView(text) {
			if (text && text.indexOf(",") > 0) {
				text = text.substring(0, text.indexOf(","))
			}
            // return window._CONFIG['imgDomainURL'] + "/" + text
            return "/jeecg-boot/" + text
        },
        // 字符串转换成数组
        stringTranarray(value){
            let a = value.split(','),
                b = [];
            for(let i=0;i<a.length;i++){
                b.push(a[i])
            }
            return b
        },
        // 把数组用逗号分隔成字符串
        commaSeparate(arr){
            let array = arr
            let str = ''
            for(let i=0;i<array.length;i++){
                if(i == array.length-1){
                    str += array[i]
                }else{
                    str += array[i]+','
                }
            }
            return str
        }
    },
}
