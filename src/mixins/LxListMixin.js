import Vue from 'vue'
import api from '@/api/company.js'
import { Message } from 'element-ui'
import Pagi from "@/views/company/components/Pagi"
import { getAction,postAction,deleteAction } from '@/api/manage'

export const LxListMixin = {
    components: {
        Pagi
    },
    created() {
        this.loadDataTable()
        // 获取单位基本信息
        this.getcompanyinfo()
    },
    data() {
        return {
            // 服务商id
            tenantId: this.$store.state.tenantId,
            // 企业基本信息
            cominfo: {},
            // 表格数据
            tableData: [],
            /* table选中的值 */
            multipleSelection: [],
            /* table加载状态 */
            loading: false,
            /* 按钮加载状态 */
            btnloading: false,
            // 分页参数
            pagi: {
                total: 0,
                current: 1,
                size: 10
            },
            /* 查询参数 */
            queryParam: {
                tenantId: this.$store.state.tenantId,
            },
            /* 排序参数 */
			isorter: {
				column: 'createTime',
				order: 'desc',
            },
            /* 初始的查询筛选参数 */
            loadingParam: {},
            /* 筛选参数 */
            filters: {},
            url: {
                list: '',
                delete: '',
                deleteBatch: ''
            }
        }
    },
    methods: {
        // 加载数据
        loadDataTable(arg){
            //加载数据 若传入参数1则加载第一页的内容
			if (arg === 1) {
				this.pagi.current = 1;
            }
            var params = this.getQueryParams();//查询条件
            this.loading = true;
            if(this.url.list != ''){
                getAction(this.url.list, params).then((res) => {
                    if (res.success) {
                        this.tableData = res.result.records;
                        this.pagi.total = res.result.total
                        this.pagi.current = res.result.current
                        this.pagi.size = res.result.size
                    }
                    this.loading = false;
                })
            }
        },
        // 查询条件
        getQueryParams() {
			//获取查询条件
			var param = Object.assign(this.queryParam, this.isorter, this.filters, this.loadingParam);
			param.pageNo = this.pagi.current;
			param.pageSize = this.pagi.size;
			return this.filterObj(param);
        },
        // 搜索
        searchQuery() {
			this.loadDataTable(1);
        },
        // 重置搜索
        searchReset() {
			this.queryParam = {
                tenantId: this.$store.state.tenantId
            }
			this.loadDataTable(1);
		},
        /**
         * 过滤对象中为空的属性
         * @param obj
         * @returns {*}
         */
        filterObj(obj) {
            if (!(typeof obj == 'object')) {
              return;
            }

            for ( var key in obj) {
              if (obj.hasOwnProperty(key)
                && (obj[key] == null || obj[key] == undefined || obj[key] === '')) {
                delete obj[key];
              }
            }
            return obj;
        },
        // 分页方法
        togglePagi(value){
            this.loading = true
            var params = this.getQueryParams();//查询条件
            params.pageNo = value
            getAction(this.url.list, params).then((res) => {
                if (res.success) {
                    this.tableData = res.result.records;
                    this.pagi.total = res.result.total
                    this.pagi.size = res.result.size
                }
                this.loading = false;
            })
        },
        // 获取单位基本信息
        getcompanyinfo(){
            let _this = this
            let tenantId = _this.tenantId
            api.getcompanytenantId({
                tenantId: _this.tenantId
            }).then(res => {
                if(res.success){
                    _this.cominfo = res.result
                    _this.verifauditStatus()
                }else{
                    _this.$router.push('/perfectinfo')
                }
            })
        },
        // 验证单位是否已经通过审核
        verifauditStatus(){
            let _this = this
            if(_this.cominfo.auditStatus == 2){
                if(!localStorage.getItem('auditpass')){
                    this.$notify({
                        title: '审核通过',
                        message: '资料审核通过!',
                        type: 'success'
                    })
                }
                localStorage.setItem('auditpass',true)
            }else if(_this.cominfo.auditStatus == 1){
                _this.$router.push('/perfectinfo')
                localStorage.removeItem('auditpass')
            }else if(_this.cominfo.auditStatus == 3){
                _this.$router.push('/perfectinfo')
                localStorage.removeItem('auditpass')
            }
        },
        // 查看
        seeview(url,row) {
            window.open(url + row.id)
        },
        // 编辑
        editNode(path,row){
            this.$router.push({path: path,query: {id: row.id}})
        },
        // 根据id编辑
        editNodeId(path,id){
            this.$router.push({path: path,query: {id: id}})
        },
        // 取消
        cancelNode(path){
            this.$router.push({path: path})
        },
        // 删除方法
        deleteNode(row){
            this.$confirm('确认删除吗?', '删除', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                if(this.url.delete){
                    deleteAction(this.url.delete,{id: row.id}).then(res => {
                        if(res.success){
                            this.$notify({
                                title: '删除成功',
                                type: 'success'
                            })
                            this.loadDataTable()
                        }
                    })
                }
            })
        },
        // 批量删除
        batchDel(){
            if (this.multipleSelection.length <= 0) {
                this.$notify({
                    title: '请选择一条数据！',
                    type: 'error'
                })
				return;
			} else {
				var ids = "";
				for (var a = 0; a < this.multipleSelection.length; a++) {
					ids += this.multipleSelection[a].id + ",";
                }
                this.$confirm('确认删除', '是否删除选中数据?', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    deleteAction(this.url.deleteBatch, { ids: ids }).then((res) => {
                        if (res.success) {
                            this.$notify({
                                title: '批量删除成功',
                                type: 'success'
                            })
                            this.loadDataTable()
                        }
                    });
                })

            }
        },
        // table选中的值
        handleSelectionChange(val) {
            this.multipleSelection = val
        },
        // 取消table选中的值
        toggleSelection(rows) {
            if (rows) {
              rows.forEach(row => {
                this.$refs.multipleTable.toggleRowSelection(row);
              });
            } else {
              this.$refs.multipleTable.clearSelection();
            }
        },
        /* 图片预览 */
		getImgView(text) {
			if (text && text.indexOf(",") > 0) {
				text = text.substring(0, text.indexOf(","))
			}
            // return window._CONFIG['imgDomainURL'] + "/" + text
            return "/jeecg-boot/" + text
        },
        // 字符串转换成数组
        stringTranarray(value){
            let a = value.split(','),
                b = [];
            for(let i=0;i<a.length;i++){
                b.push(a[i])
            }
            return b
        },
        // 把数组用逗号分隔成字符串
        commaSeparate(arr){
            let array = arr
            let str = ''
            for(let i=0;i<array.length;i++){
                if(i == array.length-1){
                    str += array[i]
                }else{
                    str += array[i]+','
                }
            }
            return str
        }
    },
}
